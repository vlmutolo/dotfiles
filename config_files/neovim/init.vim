
"Install all the plugins
call plug#begin('~/.vim/plugged')
Plug 'drewtempelmeyer/palenight.vim'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'itchyny/lightline.vim'
Plug 'racer-rust/vim-racer'
Plug 'junegunn/goyo.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'scrooloose/nerdtree'
Plug 'cespare/vim-toml'
Plug 'cstrahan/vim-capnp'
call plug#end()

" Set up a half-decent colorscheme
set background=dark
colorscheme palenight
let g:lightline = { 'colorscheme': 'palenight' }

" What is life without line numbers
set number

" Set tabs to be reasonable.
set softtabstop=4
set shiftwidth=4
set expandtab

" New splits should be on the right and bottom.
set splitright
set splitbelow
